#include "BruteForce.h"


int main()
{
	int option;
	bool ifend = 0;
	Graph* graph = nullptr;
	BruteForce* bforce = nullptr;
	string filename;

	enum Menu {
		fin,
		load,
		test
	};

	while (!ifend) {
		cout << "\n EXACT ALGORITHM \n";
		cout << "1 - load graph \n";
		cout << "2 - test algorithm \n";
		cout << "0 - end \n";
		cout << "Pick option: ";
		cin >> option;

		while (cin.fail()) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}

		switch (option) {
		case load:
			cout << "Filename: ";
			cin >> filename;
			graph = new Graph(filename);
			graph->print();
			break;
		case test:
			if (graph) {
				bforce = new BruteForce(*graph);
				bforce->print();
			}
			break;
		case fin:
			ifend = 1;
			break;
		default:
			cout << "Incorrect character entered. Please enter (0-2)." << endl;
		}
	}

    return 0;
}

