#pragma once

#include <stack>
#include <chrono>

#include "Graph.h"


class BruteForce
{
private:
	const int MAXINT = 2147483647;

	bool *is_visited;

	stack <int> cycle;
	stack <int> current_cycle;

	int vertices;
	int iterations = 0;
	double op_time = 0;
	int weight;
	int current_weight;

	Graph graph;

public:
	BruteForce(Graph graph);
	~BruteForce();
	
	void solve(int v);
	void print();
	double get_op_time();
};

