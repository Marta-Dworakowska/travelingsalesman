#include "BruteForce.h"

using namespace std::chrono;


BruteForce::BruteForce(Graph graph)
{
	this->graph = graph;
	this->vertices = graph.get_vertices();

	is_visited = new bool[vertices];
	for (int i = 0; i < vertices; i++)
		is_visited[i] = false;

	current_weight = 0;
	weight = MAXINT;

	auto start_time = high_resolution_clock::now();
		solve(0);
	auto end_time = high_resolution_clock::now();
	op_time = duration_cast<milliseconds>(end_time - start_time).count();
}


void BruteForce::solve(int v)
{
	current_cycle.push(v); // add vertice to cycle

	// do while cycle haven't got enough vertices
	if (current_cycle.size() < vertices)
	{
		is_visited[v] = true;

		// iterate trough neighbour list, ommiting visted vertices
		for (int u = 0; u < vertices; u++) {
			if (graph.edge_exist(v, u) && !is_visited[u])
			{
				current_weight += graph.get_edge(v, u);
				solve(u); // recursive with last vertice
				current_weight -= graph.get_edge(v, u);
			}
		}
		is_visited[v] = false;
	}
	// if found cycle is better than current, it becomes the solution 
	// adding last edge (creation of full cycle)
	else if (graph.get_edge(0,v))
	{
		current_weight += graph.get_edge(v, 0);	
		if (current_weight < weight)
		{
			weight = current_weight;
			cycle = current_cycle;
		}
		current_weight -= graph.get_edge(v, 0);
		iterations++; // add permutation of cycle
	}           
	current_cycle.pop();
}


BruteForce::~BruteForce()
{
	delete[] is_visited;
}


void BruteForce::print() 
{
	cout << "The minimum Hamiltonian Cycle weighs: "<< weight << endl;
	cout << "The order of vertices is: ";
	while (!cycle.empty()) {
		cout << cycle.top() << " ";
		cycle.pop();
	}
	cout << endl << "Iterations: " << iterations << endl;
	cout << "The time of work: " << op_time << " ms" << endl;
}


double BruteForce::get_op_time()
{
	return op_time;
}
