#include "Genetic.h"

using namespace std::chrono;


Genetic::Genetic(Graph graph, int population_size, int tournament_size, double mutation_propability, int time)
{
	this->population_size = population_size;
	this->tournament_size = tournament_size;
	this->mutation_propability = mutation_propability;
	this->time = time;
	this->vertices = graph.get_vertices();

	std::srand(unsigned(std::time(0)));
	random_population(graph);

	solve();
}


Genetic::~Genetic()
{
}


void Genetic::random_population(Graph graph)
{
	Cycle cycle = Cycle(graph);
	for (int i = 0; i < population_size; i++) {
		cycle.random_cycle();
		population.push_back(cycle);
	}
}


void Genetic::tournament() 
{
	// draw indexes
	set<int> numbers;
	while (numbers.size() < tournament_size)
		numbers.insert(rand() % population_size);

	// selecting two strongest subjects and one who is the weakest for removal
	auto it = numbers.begin();
	father = *it;
	mother = *it+1;
	omega = *it;
	omega2 = *it+1;

	for (; it != numbers.end(); ++it)
	{
		if (population[*it].get_weight() < population[father].get_weight()) {
			mother = father;
			father = *it; 
		}
		else if (population[*it].get_weight() < population[mother].get_weight()) {
			mother = *it; 
		}
		if (population[*it].get_weight() > population[omega].get_weight())
		{
			omega2 = omega;
			omega = *it;
		}
		else if (population[*it].get_weight() > population[omega2].get_weight())
		{
			omega2 = *it;
		}
	}
}


void Genetic::cross()
{
	// !!! PMX - Partially Mapped Crossover !!!

	// IMAAGE SECTION - drawing indexes
	// distance (1/4 amount of verices - 3/4 amount of vertices)
	int min = ceil(float(vertices) / 4);
	int max = (float(vertices) * 3) / 4;
	int interval = max - min;
	// drawning
	int begin = rand() % vertices; 
	int end = (begin + (rand() % interval + min)) % vertices;
	// remove indexes in ascending order
	if (begin > end) {
		int temp = begin;
		begin = end;
		end = temp;
	}
	// length of section
	int section_lenght = abs(begin - end);

	// TABLE OF MAPPING ALLELES
	int** map_tab = new int *[section_lenght];
	for (int i = 0, j = begin; i < section_lenght; i++, j++) {
		map_tab[i] = new int [2];
		map_tab[i][0] = population[father].get_el(j);
		map_tab[i][1] = population[mother].get_el(j);
	}

	// CREATING CHILDREN
	son = new Cycle(population[father]);
	daughter = new Cycle(population[mother]);
	// replacement of image section
	for (int i = begin; i < end; i++) {
		son->set_el(i, population[mother].get_el(i));
		daughter->set_el(i, population[father].get_el(i));
	}

	// filling remaming alleles according to mapping table
	// k - in some cases there is need to use mapping table k times (chaing of images)
	for (int k = 0; k < section_lenght; k++) {
		for (int i = 0; i < begin; i++) {
			for (int j = 0; j < section_lenght; j++) {
				if (map_tab[j][1] == son->get_el(i))
					son->set_el(i, map_tab[j][0]);
				if (map_tab[j][0] == daughter->get_el(i))
					daughter->set_el(i, map_tab[j][1]);
			}
		}
		for (int i = end; i < vertices; i++) {
			for (int j = 0; j < section_lenght; j++) {
				if (map_tab[j][1] == son->get_el(i))
					son->set_el(i, map_tab[j][0]);
				if (map_tab[j][0] == daughter->get_el(i))
					daughter->set_el(i, map_tab[j][1]);
			}
		}
	}
}


void Genetic::mutate()
{
	if (rand_double() < mutation_propability)
		son->generate_neighbor(son);
}


void Genetic::birth()
{
	population[omega] = *son;
	population[omega2] = *daughter;
}


void Genetic::solve()
{
	int op_time;
	auto start_time = high_resolution_clock::now();

	do {

		tournament();
		cross();
		mutate();
		birth();

		auto end_time = high_resolution_clock::now();
		op_time = duration_cast<milliseconds>(end_time - start_time).count();
	} while (op_time != time);

	choose_best();
}


void Genetic::choose_best()
{
	cycle = new Cycle(population[0]);

	for (int i = 1; i < population_size; i++) 
	{
		if (population[i].get_weight() < cycle->get_weight())
			cycle = &population[i];
	}
}


void Genetic::print()
{
	cycle->print();
	cout << "The time of work: " << time << " ms" << endl;
}


double Genetic::rand_double()
{
	double f = (double)rand() / RAND_MAX;
	return 0 + f * (1.0 - 0);
}
