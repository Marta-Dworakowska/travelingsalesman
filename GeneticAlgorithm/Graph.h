#pragma once
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class Graph
{
private:
	int **adjacency_matrix;
	int vertices;

public:
	Graph();
	~Graph();
	
	void load(string filename);
	void print();
	int get_vertices();
	int get_edge(int row, int column);
	bool edge_exist(int row, int column);
	bool is_empty();
};

