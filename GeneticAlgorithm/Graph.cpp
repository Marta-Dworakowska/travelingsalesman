#include "Graph.h"


Graph::Graph()
{
	this->vertices = 0;
}


void Graph::load(string filename)
{
	fstream file(filename, ios_base::in);

	if (file.good()) {
		file >> filename;
		file >> this->vertices; // setting vertices amount

		adjacency_matrix = new int *[vertices]; // create new matrix
		for (int i = 0; i < vertices; i++) {
			adjacency_matrix[i] = new int[vertices]; // initialize matrix
			for (int j = 0; j < vertices; j++)
				file >> adjacency_matrix[i][j]; // filling matrix
		}

		file.close();
	}
	else
	{
		cout << "Bad filename." << endl;
	}
}


Graph::~Graph()
{
}


void Graph::print()
{
	for (int i = 0; i < vertices; i++, cout << endl)
		for (int j = 0; j < vertices; j++)
			cout << adjacency_matrix[i][j] << " ";
}


int Graph::get_vertices() 
{
	return this->vertices;
}


int Graph::get_edge(int row, int column)
{
	return edge_exist(row,column) ? adjacency_matrix[row][column] : 0;
}


bool Graph::edge_exist(int row, int column)
{
	return adjacency_matrix[row][column] <= 0 ? 0 : 1;
}


bool Graph::is_empty()
{
	if (!adjacency_matrix) {
		return true;
	}
	return false;
}
