#include "Genetic.h"


int main()
{
	static Graph graph = Graph();

	Genetic* gen = nullptr;

	bool ifend = 0;
	int option, population_size, tournament_size, time;
	double mutation_propability;
	string filename;

	enum Menu {
		fin,
		load,
		test
	};

	while (!ifend) {
		cout << "\n GENETIC ALGORITHM \n";
		cout << "1 - load graph \n";
		cout << "2 - test algorithm \n";
		cout << "0 - end \n";
		cout << "Pick option: ";
		cin >> option;

		while (cin.fail()) {
			cout << "Incorrect character entered. Please enter (0-2): ";
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cin >> option;
		}

		switch (option) {
		case load:
			cout << "Filename: ";
			cin >> filename;
			graph.load(filename);
			graph.print();
			break;
		case test:
			if (!graph.is_empty()) {
				cout << "Population size: ";
				cin >> population_size;
				cout << "Tournament size: ";
				cin >> tournament_size;
				cout << "Mutation propability: ";
				cin >> mutation_propability;
				cout << "Time algorithm works (ms): ";
				cin >> time;
				gen = new Genetic(graph, population_size, tournament_size, mutation_propability, time);
				gen->print();
			}
			else
			{
				cout << "Graph is empty." << endl;
			}
			break;
		case fin:
			ifend = 1;
			break;
		}
	}

	return 0;
}
