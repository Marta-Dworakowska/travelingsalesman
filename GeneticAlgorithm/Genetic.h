#pragma once

#include <chrono>
#include <random>
#include <ctime>
#include <vector>
#include <set>

#include "Graph.h"
#include "Cycle.h"


class Genetic
{
private:
	// POPULATION
	vector<Cycle> population;
	Cycle* cycle;
	Cycle *son, *daughter; // offspring
	int father, mother; // max1, max2
	int omega, omega2; // min1, min2
	
	// PARAMETERS
	int population_size; 
	int tournament_size; // amount of subjects taking part in tournament
	int time; // stopying criteria - time
	double mutation_propability; // probability of mutation
	int vertices; // amount of vertices in graph

	// PRIVATE FUNCTIONS
	void random_population(Graph graph); // generation of initial population
	void tournament(); // selection of couple for procreation
	void cross(); // creating new subjects
	void mutate(); // supose mutation of one of the subjects
	void birth(); // adding new subject to population

public:
	Genetic(Graph graph, int population_size, int tournament_size, double mutation_propability, int time_ms);
	~Genetic();

	void solve();
	void choose_best();
	void print();
	double rand_double();
};

