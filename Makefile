# Exact
exact:
	g++ ExactAlgorithm/ExactAlgorithm.cpp ExactAlgorithm/BruteForce.cpp \
	    ExactAlgorithm/Graph.cpp -o ExactRun ; ./ExactRun

# Local
local:
	g++ LocalSearch/LocalSearch.cpp LocalSearch/Cycle.cpp \
	    LocalSearch/Graph.cpp LocalSearch/Annealing.cpp -o LocalRun ; ./LocalRun

# Genetic
genetic:
	g++ GeneticAlgorithm/GeneticAlgorithm.cpp GeneticAlgorithm/Cycle.cpp \
	    GeneticAlgorithm/Graph.cpp GeneticAlgorithm/Genetic.cpp -o GeneticRun ; ./GeneticRun
