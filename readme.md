# Traveling Salesman Problem and algorithms to solve it

![](con.png)

Console app that solves TSP trough few approaches: 

- Exact Algorithm - Brute Force

- Local Search - Simmulated Annealing

- Genetic Algorithm

and shows time of calculation based on parameteres of algorithms.

/data folder contains examples of graphs.

### Parameters of algorithms

App allows to set parameteres of algorithms.

#### Local Search

![](local.png)

1. Amount of interations one temperature lasts - decides how many local solutions (on one temperature) is checked
2. Amount of temperatures which dosen't change solution - stoping criteria which means if temperature dosen't change for long time algorithm may stop
3. Cooling rate - how rapidly temperature is lowering

With implementation of this algorithm i was suggested by film: https://youtu.be/gX-X85dCib0.

#### Genetic Algorithm

![](gen.png)

1. Population size - how many subject is in every interation
2. Tournament size - how many subject participate in selection
3. Time of work - stopying criteria deciding how optimum solution is

## Prerequisites

Project requires:

- g++ 7.5.0

- c++14


## Installation
```bash
git clone https://Marta-Dworakowska@bitbucket.org/Marta-Dworakowska/pea.git
```

## Usage
Launching exact algorithm:
```bash
./ExactRun
```
Launching local search algorithm:
```bash
./LocalRun
```
Launching genetic algorithm:
```bash
./GeneticRun
```