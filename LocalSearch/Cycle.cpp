#include "Cycle.h"


Cycle::Cycle(Graph &graph)
{
	this->graph = graph;
	this->vertices = graph.get_vertices();

	tab = new int[vertices];
	for (int i = 0; i < vertices; ++i)
		tab[i] = i;

	weight = 0;
	for (int i = 0; i < vertices - 1; i++)
		weight += graph.get_edge(tab[i], tab[i + 1]);
	weight += graph.get_edge(tab[vertices - 1], tab[0]);
}


Cycle::Cycle(const Cycle & cycle)
{
	this->graph = cycle.graph;
	this->vertices = graph.get_vertices();
	this->weight = cycle.weight;

	tab = new int[vertices];
	for (int i = 0; i < vertices; ++i)
		tab[i] = cycle.tab[i];
}


Cycle::~Cycle()
{
	delete[] tab;
}


Cycle & Cycle::operator=(const Cycle & cycle)
{
	// check for self-assignment
	if (&cycle == this)
		return *this;
	// reuse storage when possible
	this->graph = cycle.graph;
	this->vertices = graph.get_vertices();
	this->weight = cycle.weight;

	this->tab = new int[vertices];
	for (int i = 0; i < vertices; ++i)
		this->tab[i] = cycle.tab[i];

	return *this;
}


void Cycle::random_cycle()
{
	random_shuffle(tab, tab + vertices); // set cycle
	weight = 0; // set weight
	for (int i = 0; i < vertices - 1; i++)
		weight += graph.get_edge(tab[i], tab[i + 1]);
	weight += graph.get_edge(tab[vertices - 1], tab[0]);
}


void Cycle::generate_neighbor(Cycle * cycle)
{
	// copying given cycle to memeory of current
	for (int i = 0; i < vertices; i++)
		this->tab[i] = cycle->get_el(i);
	this->weight = cycle->get_weight();

	// replace two random vertices 
	// draw random index of table ( random_v < random_u )
	int random_v, random_u;
	do {
		random_v = rand() % (vertices - 1);
		random_u = rand() % (vertices - 1);

		if (random_v > random_u)
		{
			int temp = random_v;
			random_v = random_u;
			random_u = random_v;

		}
	} while (random_v == random_u);


	// swap cycle in table
	int temp_v = tab[random_v];
	int temp_u = tab[random_u];

	// calculate new weight - for defined in that way neighborhood only 4 edges are chaning
	// substract old 4 edges from new cost
	weight -= graph.get_edge(tab[(random_v - 1 + vertices) % vertices] , temp_v);
	weight -= graph.get_edge(temp_v, tab[(random_v + 1) % vertices]);
	if (abs(random_v - random_u) != 1) // special case: vertices next to each other
		weight -= graph.get_edge(tab[(random_u - 1 + vertices) % vertices] , temp_u);
	if(abs(random_v - random_u) != vertices - 1) // special case: first and last vertice
		weight -= graph.get_edge(temp_u, tab[(random_u + 1) % vertices]);

	tab[random_v] = temp_u;
	tab[random_u] = temp_v;

	// add for new cost 4 new edges
	weight += graph.get_edge(tab[(random_v - 1 + vertices) % vertices], tab[random_v]);
	weight += graph.get_edge(tab[random_v], tab[(random_v + 1) % vertices]);
	if (abs(random_v - random_u) != 1)
		weight += graph.get_edge(tab[(random_u - 1 + vertices) % vertices], tab[random_u]);
	if (abs(random_v - random_u) != vertices - 1)
		weight += graph.get_edge(tab[random_u], tab[(random_u + 1) % vertices]);
}


int Cycle::get_weight()
{
	return weight;
}


int Cycle::get_el(int order)
{
	return tab[order];
}


void Cycle::print()
{
	cout << "The order of vertices is: ";
	for (int i = 0; i < vertices; i++)
		cout << tab[i] << " ";
	cout << tab[0] << endl;
	cout << "Weight: " << weight << endl;
}
