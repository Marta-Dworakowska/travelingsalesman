// simulated annealing

// - selection of the initial temperature - FOR THE LANDSCAPE OF THE  OBJECTIVE FUNCTION 
// - selection of the neighborhood function - AS MUCH AS POSSIBLE SMALL DIFFERENCES OF COST BETWEEN NEIGHBORS - A SOFT LANDSCAPE
// - selection of the cooling function - EXPOSURE TO 0 - f (x) = f (x-1) * 0.9
// - the choice of the length of one temperature lasting - THE SAME ROW AS NEIGHBORHOOD FUNCTION
// - selection of the stop condition - THE SOLUTION WILL NOT CHANGE FOR SEVERAL TEMPERATURES
// * generate random neighbor (i.e. specific cycle permutations) and
// * if improvement take it and if deterioration accept it or not
// * cooling

#include "Annealing.h"


int main()
{
	Graph* graph = nullptr;
	Annealing* anneal = nullptr;

	int option;
	bool ifend = 0;
	int temp_last, max_temp_changes;
	double cool_rate;
	string filename;

	enum Menu {
		fin,
		load,
		test
	};

	while (!ifend) {
		cout << "\n LOCAL SEARCH \n";
		cout << "1 - load graph \n";
		cout << "2 - test algorithm \n";
		cout << "0 - end \n";
		cout << "Pick option: ";
		cin >> option;

		while (cin.fail()) {
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}

		switch (option) {
		case load:
			cout << "Filename: ";
			cin >> filename;
			graph = new Graph(filename);
			graph->print();
			break;
		case test:
			if (graph) {
				cout << "Amount of iterations one temperature last: ";
				cin >> temp_last;
				cout << "Amount of temperatures which doesn't change solution: ";
				cin >> max_temp_changes;
				cout << "Cooling rate (0-1): ";
				cin >> cool_rate;
				anneal = new Annealing(*graph, temp_last, max_temp_changes, cool_rate);
				anneal->print();
			}
			break;
		case fin:
			ifend = 1;
			break;
		default:
			cout << "Incorrect character entered. Please enter (0-2)." << endl;
		}
	}

	return 0;
}


