#pragma once

#include <random>
#include <iostream>
#include <algorithm>

#include "Graph.h"


class Cycle
{
private:
	int* tab;

	int weight;
	int vertices;
	Graph graph;

public:
	Cycle(Graph& graph);
	Cycle(const Cycle& cycle);
	~Cycle();

	Cycle & operator=(const Cycle & cycle);

	void random_cycle();
	void generate_neighbor(Cycle * cycle);
	int get_weight();
	int get_el(int order);
	void print();
};

