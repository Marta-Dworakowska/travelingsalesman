#pragma once

#include <stack>
#include <chrono>
#include <ctime>
#include <random>
#include <math.h>

#include "Graph.h"
#include "Cycle.h"


class Annealing
{
private:
	Cycle* cycle;

	int op_time = 0;
	double temp; // temperature
	int temp_last; // decision how many interation one temperature last
	int max_temp_changes; // decision after how many changes of temperature, which doesn't changed the solution algorithm stops
	double cool_rate; // cooling factor
	int max_delta; // max occuring in graph difference between cycle and it's neighbor
	double acceptance_param0; // scaling factor - max delta

	void generate_temp0(); // djucst inistial temperature for landscape of func
	void calc_max_delta(); // calculate max_delta
	void lower_temp(); // lower temperature
	double rand_double();
	
public:
	Annealing(Graph graph, int temp_last, int max_temp_changes, double cool_rate);
	~Annealing();

	void solve();
	void print();
	int get_op_time();
};