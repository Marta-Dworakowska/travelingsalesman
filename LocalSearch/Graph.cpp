#include "Graph.h"


Graph::Graph(string filename)
{
	fstream file(filename, ios_base::in);

	if (file.good()) {
		file >> filename; // filename
		file >> this->vertices; // setting vertices amount

		adjacency_matrix = new int *[vertices]; // initialize matrix
		for (int i = 0; i < vertices; i++)
			adjacency_matrix[i] = new int[vertices];

		// filling matrix
		for (int i = 0; i < vertices; i++)
			for (int j = 0; j < vertices; j++) 
				file >> adjacency_matrix[i][j];

		file.close();
	}
}


Graph::~Graph()
{
	for (int i = 0; i < vertices; i++)
		delete[] adjacency_matrix[i];
	delete[] adjacency_matrix;
}


void Graph::print()
{
	for (int i = 0; i < vertices; i++, cout << endl)
		for (int j = 0; j < vertices; j++)
			cout << adjacency_matrix[i][j] << " ";
}


int Graph::get_vertices() 
{
	return this->vertices;
}


int Graph::get_edge(int row, int column)
{
	return edge_exist(row,column) ? adjacency_matrix[row][column] : 0;
}


bool Graph::edge_exist(int row, int column)
{
	return adjacency_matrix[row][column] <= 0 ? 0 : 1;
}
