#include "Annealing.h"

using namespace std::chrono;


Annealing::Annealing(Graph graph, int temp_last, int max_temp_changes, double cool_rate) 
{
	std::srand(unsigned(std::time(0)));
	cycle = new Cycle(graph);

	temp = 0.0;
	max_delta = 0;
	acceptance_param0 = 0.99; // how willingly in initila temperature accept deterioration of the solution

	// ------------------- to set -------------------------------------------------------------------------------
	/* how long one temperature lasts */				this->temp_last = temp_last;
	/* how many not-chaning solution temperatures */	this->max_temp_changes = max_temp_changes;
	/* how many times lower temperature */				this->cool_rate = cool_rate;
	// ----------------------------------------------------------------------------------------------------------

	generate_temp0(); //based on max_delta, temp, acceptance_param
	cycle->random_cycle();

	auto start_time = high_resolution_clock::now();
	solve();
	auto end_time = high_resolution_clock::now();
	op_time = duration_cast<milliseconds>(end_time - start_time).count();
}


Annealing::~Annealing()
{
}


void Annealing::solve()
{
	Cycle* current_cycle = new Cycle(*cycle);
	// do while temp_changes < max_temp_changes
	for (int temp_changes = 0; temp_changes <= max_temp_changes;) {
		int prev_weight = cycle->get_weight();
		for (int i = 0; i < temp_last; i++) {
			current_cycle->generate_neighbor(cycle); // generate neighbor of current solution
			// if solution is better accept it
			if (current_cycle->get_weight() <= cycle->get_weight()) {
				*cycle = *current_cycle;
			}
			// if solution is worse make a decision basend on factor
			else if (exp(-(current_cycle->get_weight() - cycle->get_weight()) / temp) > rand_double())
			{
				*cycle = *current_cycle;
			}
		}
		lower_temp(); // cooling

		if (cycle->get_weight() == prev_weight)
			temp_changes++;
		else
			temp_changes = 0;
	}
}


void Annealing::generate_temp0()
{
	calc_max_delta(); // mark max diffrenence between cycle and its neighbor
	temp = abs(max_delta/log(acceptance_param0)); // based on that difference calculate temperature
}


double Annealing::rand_double() 
{
	double f = (double)rand() / RAND_MAX;
	return 0 + f * (1.0 - 0);
}


void Annealing::calc_max_delta() 
{
	Cycle* current_cycle = new Cycle(*cycle);
	// draw 100 cycles
	// draw for each cycle neighbor
	// calculate difference between cycle and its neighbor
	// select max diffrence
	for (int i = 0; i < 100; i++) {
		int current_max_delta = 0;

		cycle->random_cycle();
		current_cycle->generate_neighbor(cycle);

		current_max_delta = abs(current_cycle->get_weight() - cycle->get_weight());

		if (current_max_delta > max_delta)
			max_delta = current_max_delta;
	}
}


void Annealing::lower_temp()
{
	temp *= cool_rate;
}


void Annealing::print()
{
	cycle->print();
	cout << "The time of work: " << op_time << " ms" << endl;
}


int Annealing::get_op_time()
{
	return op_time;
}
